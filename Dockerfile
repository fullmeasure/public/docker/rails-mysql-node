FROM ruby:2.3.6-alpine3.4
MAINTAINER FME

RUN mkdir /current

WORKDIR /current

ADD . /current

RUN apk add --update --no-cache \
      build-base \
      nodejs \
      tzdata \
      git \
      libxml2-dev \
      libxslt-dev \
      sqlite-dev \
      mysql-dev \
      curl \
      linux-headers

RUN bundle config build.nokogiri --use-system-libraries


EXPOSE 3000

CMD ["/current/docker-entrypoint.sh"]
